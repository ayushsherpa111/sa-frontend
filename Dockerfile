FROM node:19-alpine as builder

WORKDIR /app

COPY . .

RUN npm install 
RUN npm run build
RUN npm config set fetch-retry-mintimeout 20000
RUN npm config set fetch-retry-maxtimeout 120000

FROM nginx:alpine

WORKDIR /usr/share/nginx/html

RUN rm -rf ./*

COPY --from=builder /app/build .

EXPOSE 3000

ENTRYPOINT ["nginx", "-g", "daemon off;"]
