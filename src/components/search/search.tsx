import React, { useState } from "react";

export function SearchComponent() {
  let [searchField, setSearchField] = useState(""); 
  return (
    <div>
      <p>Search</p>
      <input
        type="text"
        value={searchField}
        placeholder="Search"
        onChange={(event) => setSearchField(event.target.value)}
      />
      <button
        onClick={() =>
          fetch("/api/analyze", {
            method: "POST",
            mode: "cors",
            headers: {
              "Content-Type": "application/json",
              "Access-Control-Request-Method": "POST",
            },
            body: JSON.stringify({
              value: searchField,
            }),
          })
            .then((res) => res.json())
            .then(console.log)
        }
      >
        Submit
      </button>
    </div>
  );
}
