import React from "react";
import { SearchComponent } from "./components/search/search";
import "./App.css";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <SearchComponent />
      </header>
    </div>
  );
}

export default App;
